package change;

class Changes {

    int chg = 0;
    int[] bank = { 1000, 500, 100, 50, 20, 10, 5, 1 };

    public Changes() {

    }

    public void Charge(int money) {
        int num = money;
        for (int i : bank) {
            if (num >= i) {
                chg = num / i;
                num %= i;
                System.out.println("Money :: " + i + " :: Total :: " + chg);
            }
        }
    }

    public void Change(int number) {
        Struct str = new Struct();
        int num = number;

        if (num >= 1000) {
            str.amt_1000 = num / 1000;
            num %= 1000;

            System.out.println("Money :: 1000 :: Total :: " + str.amt_1000);
        }

        if (num >= 500) {
            str.amt_500 = num / 500;
            num %= 500;

            System.out.println("Money :: 500 :: Total :: " + str.amt_500);
        }
        if (num >= 100) {
            str.amt_100 = num / 100;
            num %= 100;

            System.out.println("Money :: 100 :: Total :: " + str.amt_100);
        }
        if (num >= 50) {
            str.amt_50 = num / 50;

            System.out.println("Money :: 50 :: Total :: " + str.amt_50);
            num %= 50;
        }
        if (num >= 20) {
            str.amt_20 = num / 20;
            num %= 20;

            System.out.println("Money :: 20 :: Total :: " + str.amt_20);
        }
        if (num >= 10) {
            str.amt_10 = num / 10;
            num %= 10;

            System.out.println("Money :: 10 :: Total :: " + str.amt_10);
        }
        if (num >= 5) {
            str.amt_5 = num / 5;
            num %= 5;

            System.out.println("Money :: 5 :: Total :: " + str.amt_5);
        }
        if (num >= 1) {
            str.amt_1 = num / 1;
            num %= 1;

            System.out.println("Money :: 1 :: Total :: " + str.amt_1);
        }

        if (num == -1) {
            System.out.println("============= END =============");
        }

    }

}